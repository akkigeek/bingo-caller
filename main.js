let number_calling_intrv_time_in_sec = 3000
const NUMBERS_NICKNAMES = [
    "Kellys eye",
    "One little duck",
    "Cup of tea",
    "Knock at the door",
    "Man alive",
    "Tom Mix a dozen",
    "Lucky seven",
    "Garden gate",
    "Doctors orders",
    "Prime Ministers name’s den",
    "Legs eleven",
    "One dozen",
    "Unlucky for some",
    "Valentine’s Day",
    "Young and keen",
    "Sweet 16 and never been kissed",
    "Dancing queen",
    "Coming of age",
    "Goodbye teens",
    "One score",
    "Royal salute of the door",
    "Two little ducks",
    "Thee and me",
    "Two dozen",
    "Duck and dive",
    "Pick and mix",
    "Gateway to heaven",
    "In a Over weight",
    "Rise and shine",
    "Dirty Gertie",
    "Get up and run",
    "Buckle my shoe",
    "Dirty knee the threes chips and peas",
    "Ask for more",
    "Jump and jive",
    "Three dozen",
    "More than eleven",
    "Christmas cake",
    "39 steps",
    "Life begins",
    "Time for fun",
    "Winnie the Pooh",
    "Down on your knees",
    "Droopy drawers",
    "Halfway there",
    "Up to tricks",
    "Four and seven",
    "Four dozen",
    "PC",
    "Half a century",
    "Tweak of the thumb",
    "Danny La Rue",
    "Here comes Herbie in a tree",
    "Clean the floor",
    "Snakes alive",
    "Shotts Bus",
    "Heinz varieties",
    "Make them wait",
    "Brighton Line",
    "Five dozen",
    "Baker’s bun",
    "Turn the Tickety-boo",
    "Tickle me 63",
    "Red raw",
    "Old age pension",
    "Clickety click",
    "Stairway to heaven",
    "Saving Grace",
    "Favourite of mine",
    "Three score and ten",
    "Bang on the drum",
    "Six dozen",
    "Queen bee",
    "Hit the floor",
    "Strive and strive",
    "Trombones",
    "Sunset strip",
    "39 more steps",
    "One more time",
    "Eight and blank",
    "Stop and run",
    "Straight on through",
    "Time for tea",
    "Seven dozen",
    "Staying alive",
    "Between the sticks",
    "Torquay in Devon",
    "Two fat ladies",
    "Nearly there",
    "Top of the shop"
]
let BINGO_NUMBERS = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90
]


let btnstart,btnpause,numberboard,game_int
domReady(function(){
    btnstart = document.querySelector("#btnstart")
    btnpause = document.querySelector("#btnpause")
    numberboard = document.querySelector(".numbers")

    BINGO_NUMBERS.forEach(num => {
        div = document.createElement("div")
        div.innerHTML = num
        div.classList.add("item")
        div.id = `id_${num}`
        numberboard.appendChild(div)
    })
    startGame()

    // document.querySelector('#button').click();
    
})

function startGame(){

    btnstart.style.display = "none"
    btnpause.style.display = "inline-block"

    game_int = setInterval(() => {
        if(BINGO_NUMBERS.length > 0){
            let rand_num = Math.floor(Math.random() * BINGO_NUMBERS.length)
            let num = BINGO_NUMBERS.splice(rand_num,1)
            document.querySelector(`#id_${num}`).classList.add("done")
            document.querySelector(".numbershow").innerHTML = num

            var msg = new SpeechSynthesisUtterance();
            msg.text = `${num} ${NUMBERS_NICKNAMES[num]}` //"Hello World";
            window.speechSynthesis.cancel();
            window.speechSynthesis.speak(msg);
        }
        else clearInterval(game_int)
    },number_calling_intrv_time_in_sec)

}
function pauseGame(){
    btnpause.style.display = "none"
    btnstart.style.display = "inline-block"
    if(game_int)clearInterval(game_int)
}
function resetGame(){ window.location.reload()}
function finishGame(){ window.location.href = "index.html"}

function domReady(fn) {
    document.addEventListener("DOMContentLoaded", fn);
    if (document.readyState === "interactive" || document.readyState === "complete" ) {
      fn();
    }
}


